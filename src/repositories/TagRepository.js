import HTTP from "@/common/http";

async function getTags() {
  const response = await HTTP.get("tags");
  return response.data;
}

async function getOneTag(id) {
  const response = await HTTP.get(`tags/${id}`);
  return response.data;
}

async function addTag(tag) {
  const response = await HTTP.post("tags", tag);
  return response.data;
}

async function updateTag(id, tag) {
  const response = await HTTP.put(`tags/${id}`, tag);
  return response.data;
}

async function deleteTag(id) {
  const response = await HTTP.delete(`tags/${id}`);
  return response;
}

export default {
  getTags,
  getOneTag,
  addTag,
  updateTag,
  deleteTag,
};
