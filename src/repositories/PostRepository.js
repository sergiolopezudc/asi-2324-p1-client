import HTTP from "@/common/http";

const resource = "posts";

function applyDate(post) {
  post.timestamp = new Date(post.timestamp);
  return post;
}

export default {
  async findAll(query, sort) {
    const params = new URLSearchParams();
    if (query) params.append("query", query);
    if (sort) params.append("sort", sort);
    const paramsStr = params.toString();
    let url = resource;
    if (paramsStr) url += "?" + paramsStr;
    const response = await HTTP.get(url);
    response.data.forEach(applyDate);
    return response.data;
  },

  // async findAllByUser(username, query, sort) {
  //   const params = new URLSearchParams();
  //   if (query) params.append("query", query);
  //   if (sort) params.append("sort", sort);
  //   if (username) params.append("author.login", username);
  //   const paramsStr = params.toString();
  //   let url = resource;
  //   if (paramsStr) url += "?" + paramsStr;
  //   const response = await HTTP.get(url);
  //   response.data.forEach(applyDate);
  //   return response.data;
  // },

  // async findAllByTag(tagId) {
  //   // const params = new URLSearchParams();
  //   // if (query) params.append("query", query);
  //   // if (sort) params.append("sort", sort);
  //   // if (username) params.append("author.login", username);
  //   // const paramsStr = params.toString();
  //   // let url = resource;
  //   // if (paramsStr) url += "?" + paramsStr;
  //   const response = await HTTP.get("posts");
  //   // const response = await HTTP.get(url);
  //   response.data.forEach(applyDate);
  //   const postsByTag = response.data.filter((post) => {
  //     return post.tags.some((tag) => tag.id === tagId);
  //   });
  //   return postsByTag;
  // },

  async findOne(id) {
    return (await HTTP.get(`${resource}/${id}`)).data;
  },

  async save(post) {
    if (post.id) {
      return applyDate((await HTTP.put(`${resource}/${post.id}`, post)).data);
    } else {
      return applyDate((await HTTP.post(`${resource}`, post)).data);
    }
  },

  async delete(id) {
    return await HTTP.delete(`${resource}/${id}`);
  },
};
