import HTTP from "@/common/http";

async function getAllUsers() {
  const response = await HTTP.get("users");
  return response.data;
}

async function deactivateUser(id) {
  const response = await HTTP.delete(`users/${id}/active`);
  return response.data;
}

async function activateUser(id) {
  const response = await HTTP.put(`users/${id}/active`);
  return response.data;
}

async function deleteUser(id) {
  const response = await HTTP.delete(`users/${id}`);
  return response;
}

export default {
  getAllUsers,
  deactivateUser,
  activateUser,
  deleteUser,
};
