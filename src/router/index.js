import { createRouter, createWebHistory } from "vue-router";
import PostList from "../components/PostList.vue";
import PostDetail from "../components/PostDetail.vue";
import PostForm from "../components/PostForm.vue";
import UserList from "../components/UserList.vue";
import UserProfile from "../components/UserProfile.vue";
import TagList from "../components/TagList.vue";
import TagForm from "../components/TagForm.vue";
import PostListByTag from "../components/PostListByTag.vue";
import RegisterForm from "../components/RegisterForm.vue";
import LoginForm from "../components/LoginForm.vue";
import ErrorNotFoundView from "../views/ErrorNotFoundView.vue";

import auth from "@/common/auth";
import { getStore } from "@/common/store";

const routes = [
  {
    path: "/",
    redirect: "/posts",
  },
  {
    path: "/posts",
    name: "PostList",
    component: PostList,
    meta: { public: true },
  },
  {
    name: "PostCreate",
    path: "/posts/new",
    component: PostForm,
    meta: { requiresAuth: true },
  },
  {
    path: "/posts/:postId",
    name: "PostDetail",
    component: PostDetail,
    meta: { public: true },
  },
  {
    path: "/posts/edit/:postId",
    name: "PostEdit",
    component: PostForm,
    meta: { requiresAuth: true },
  },
  {
    path: "/authors",
    name: "UserList",
    component: UserList,
    meta: { public: true },
  },
  {
    path: "/authors/:username",
    name: "UserProfile",
    component: UserProfile,
    meta: { public: true },
  },
  {
    path: "/tags",
    name: "TagList",
    component: TagList,
    meta: { public: true },
  },
  {
    path: "/tags/new",
    name: "TagForm",
    component: TagForm,
    meta: { requiresAuth: true, authority: "ADMIN" },
  },
  {
    path: "/tags/edit/:tagId",
    name: "TagEdit",
    component: TagForm,
    meta: { requiresAuth: true, authority: "ADMIN" },
  },
  {
    path: "/postsbytag/:tagId",
    name: "PostListByTag",
    component: PostListByTag,
    meta: { public: true },
  },

  {
    path: "/register",
    name: "RegisterForm",
    component: RegisterForm,
    meta: { public: true, isLoginPage: true },
  },
  {
    path: "/login",
    name: "LoginForm",
    component: LoginForm,
    meta: { public: true, isLoginPage: true },
  },
  {
    path: "/:catchAll(.*)*",
    component: ErrorNotFoundView,
    meta: { public: true },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  // Lo primero que hacemos antes de cargar ninguna ruta es comprobar si el usuario está autenticado (revisando el token)
  auth.isAuthenticationChecked.finally(() => {
    // por defecto, el usuario debe estar autenticado para acceder a las rutas
    const requiresAuth = !to.meta.public;

    const requiredAuthority = to.meta.authority;
    const userIsLogged = getStore().state.user.logged;
    const loggedUserAuthority = getStore().state.user.authority;

    if (requiresAuth) {
      // página privada
      if (userIsLogged) {
        if (requiredAuthority && requiredAuthority != loggedUserAuthority) {
          // usuario logueado pero sin permisos suficientes, le redirigimos a la página de login
          alert(
            "Acceso prohibido para el usuario actual; intenta autenticarte de nuevo"
          );
          auth.logout();
          next("/login");
        } else {
          // usuario logueado y con permisos adecuados
          next();
        }
      } else {
        // usuario no está logueado, no puede acceder a la página
        alert("Esta página requiere autenticación");
        next("/login");
      }
    } else {
      // página pública
      if (userIsLogged && to.meta.isLoginPage) {
        // si estamos logueados no hace falta volver a mostrar el login
        next({ name: "Home", replace: true });
      } else {
        next();
      }
    }
  });
});

export default router;
